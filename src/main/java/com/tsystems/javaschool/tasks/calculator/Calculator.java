package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Stack;

public class Calculator {

    public static boolean isDouble(String s) {
        try {
            Double.valueOf(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isOperation(char character) {
        if (character == '+' || character == '*' ||character == '/' ||character == '-'){
            return true;
        }
        return false;
    }

    public static boolean isValid(String statement){
        int parenthesisLevel = 0;
        for (int i = 0; i < statement.length(); i++)
        {
            char elem = statement.charAt(i);
            if (i == 0){
                if (!Character.isDigit(elem) && elem != '-' && elem != '('){
                    return false;
                }
                continue;
            }
            if (i == statement.length() - 1){
                if (!Character.isDigit(elem) && elem != ')'){
                    return false;
                }
                continue;
            }

            if (Character.isDigit(elem)){
                if(statement.charAt(i + 1) == '(')
                    return false;
            }
            else if (isOperation(elem)){
                if (elem == '-'){ // если унарный
                    if (Character.isDigit(statement.charAt(i + 1)) && statement.charAt(i - 1) == '(') {
                        continue;
                    }
                }
                if (!(Character.isDigit(statement.charAt(i - 1)) ||  statement.charAt(i - 1) == ')') || !(statement.charAt(i + 1) == '(' || Character.isDigit(statement.charAt(i + 1)))){
                    return false;
                }
            }
            else if (elem == '.'){
                if (!Character.isDigit(statement.charAt(i - 1)) || !Character.isDigit(statement.charAt(i + 1))){
                    return false;
                }
            }
            else if (elem == '('){
                parenthesisLevel++;
                if (!(Character.isDigit(statement.charAt(i - 1)) ||  isOperation(statement.charAt(i - 1))) || !(Character.isDigit(statement.charAt(i + 1)) ||  isOperation(statement.charAt(i + 1)))){
                    return false;
                }
            }
            else if (elem == ')'){
                if (--parenthesisLevel < 0 || !Character.isDigit(statement.charAt(i - 1)) || !isOperation(statement.charAt(i + 1))){
                    return false;
                }
            }
            else{
                return false;
            }
        }
        if (parenthesisLevel == 0)
            return true;
        return false;
    }

    public static int getPriority(String s) {
        if (s.equals("(") || s.equals(")")){
            return 3;
        }
        else if (s.equals("*") || s.equals("/")){
            return 1;
        }
        else // if (s == "+" || s == "-")
        {
            return 2;
        }
    }

    public static String evaluate(String statement) {
        if (statement == null || statement.equals("") || !isValid(statement)) {
            return null;
        }

        Stack<String> operators = new Stack<String>();
        ArrayDeque<String> result = new ArrayDeque<String>();

        String expression = new String(statement);
        boolean last = true; // true - начало выражения или последний токен - открывающая скобка
        expression = expression.replace("+", " + ").replace("-", " - ").replace("*", " * ").replace("/", " / ")
                .replace("(", " ( ").replace(")", " ) ").replace("  ", " ");

        if (expression.charAt(0) == ' ') {
            expression = expression.substring(1);
        }

        String[] tokens = expression.split(" ");

        for (int i = 0; i < tokens.length; i++) {
            if (isDouble(tokens[i])) { // если число
                result.add(tokens[i]);
                last = false;
            } else if (last == true && tokens[i].equals("-")) { // если унарный минус
                operators.push("u-");
                last = false;
            } else if (tokens[i].equals("(")) { // если откр скобка
                last = true;
                operators.push(tokens[i]);
            } else if (tokens[i].equals(")")) { // если закр скобка
                last = false;
                while (!operators.peek().equals("(")) {
                    result.add(operators.pop());
                }
                operators.pop();
            } else { // если бин операция
                last = false;
                while (!operators.isEmpty() && (operators.peek().equals("u-") || getPriority(operators.peek()) <= getPriority(tokens[i]))) {
                    result.add(operators.pop());
                }
                operators.push(tokens[i]);
            }
        }

        while (!operators.isEmpty()) {
            result.add(operators.pop());
        }

        Stack<Double> calculate = new Stack<Double>();
        double sum = 0;

        while (!result.isEmpty()) {
            if (isDouble(result.peek())) {
                calculate.push(Double.valueOf(result.pop()));
            } else {
                String operation = result.pop();
                if (operation.equals("+")) {
                    calculate.push(Double.valueOf(calculate.pop()) + Double.valueOf(calculate.pop()));
                } else if (operation.equals("-")) {
                    calculate.push(-Double.valueOf(calculate.pop()) + Double.valueOf(calculate.pop()));
                } else if (operation.equals("*")) {
                    calculate.push(Double.valueOf(calculate.pop()) * Double.valueOf(calculate.pop()));
                } else if (operation.equals("/")) {
                    double x2 = calculate.pop();
                    double x1 = calculate.pop();
                    if (x2 == 0){
                        return null;
                    }
                    calculate.push(x1/x2);
                } else if (operation.equals("u-")) {
                    calculate.push(Double.valueOf(calculate.pop()));
                }
            }
        }
        double res = calculate.pop();
        if (res == (int) res){
            return String.valueOf((int) res);
        }
        else{
            return String.valueOf((double) Math.round(res * 10000) / 10000);
        }
    }

    public static void main(String[] args){
        System.out.println(evaluate("(12*(5-1)"));
    }

}

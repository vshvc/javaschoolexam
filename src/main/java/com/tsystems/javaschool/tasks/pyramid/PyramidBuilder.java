package com.tsystems.javaschool.tasks.pyramid;

import java.sql.Array;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    public static void qsort(Integer[] array, int first, int last){
        int pivot = array[(first + last) / 2];
        int currentFirst = first;
        int currentLast = last;
        while (currentFirst <= currentLast){
            while (array[currentFirst] < pivot) currentFirst++;
            while (array[currentLast] > pivot) currentLast--;
            if (currentFirst <= currentLast){
                int temp = array[currentFirst];
                array[currentFirst] = array[currentLast];
                array[currentLast] = temp;
                currentFirst++;
                currentLast--;
            }
        }
        if (first < currentLast)
            qsort(array, first, currentLast);
        if (last > currentFirst)
            qsort(array, currentFirst, last);
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int size = inputNumbers.size();
        boolean isGood = false;
        int i = 1, j = 1;
        for (int goodSize = 1, n = 2; goodSize <= size; goodSize += n++, j+=2, i++)
        {
            if (goodSize == size) {
                isGood = true;
                break;
            }
            else if (goodSize < 0){
                break;
            }
        }
        if (!isGood || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        Integer[] inputNumbersArray = new Integer[size];
        for (int k = 0; k < size; k++)
            inputNumbersArray[k] = inputNumbers.get(k);

        qsort(inputNumbersArray, 0, size - 1);

        int[][] res = new int[i][j];

        int number = 0;

        for(int k = 0; k < i; k++){
            int start = (j - 2*k - 1) / 2;
            for (int m = start; m < start + k * 2 + 1; m += 2){
                res[k][m] = inputNumbersArray[number++];
            }
        }

        return res;
    }
}

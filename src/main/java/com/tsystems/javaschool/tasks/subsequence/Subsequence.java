package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        int xIndex = 0;
        int yIndex = 0;
        if (x == null || y == null)
            throw new IllegalArgumentException();
        while (xIndex < x.size() && yIndex < y.size()){
            if (x.get(xIndex).equals(y.get(yIndex))){
                xIndex++;
                yIndex++;
            }
            else{
                yIndex++;
            }
        }
        return (xIndex >= x.size());
    }
}
